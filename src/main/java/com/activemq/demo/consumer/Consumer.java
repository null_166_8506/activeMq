package com.activemq.demo.consumer;

import com.activemq.demo.message.MessageModel;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * 消费者
 */
@Component
public class Consumer {

    @JmsListener(destination = "default.queue")
    public void receiveQueue(MessageModel message) {
        System.out.println("收到消息" + message.toString());
    }
}