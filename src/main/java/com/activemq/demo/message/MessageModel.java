package com.activemq.demo.message;
 
import com.activemq.demo.utils.DateUtil;
import lombok.*;
 
import java.io.Serializable;
import java.util.Date;
 
/**
 * 消息模型
 */
@Data
public class MessageModel implements Serializable {
 
    private String titile;
    private String message;
    private Object data;
 
    @Override
    public String toString() {
        return "MessageModel{" +
                "titile='" + titile + '\'' +
                ", message='" + message + '\'' +
                ", data='" + data.toString() + '\'' +
                ", date=" + DateUtil.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss") +
                '}';
    }
}