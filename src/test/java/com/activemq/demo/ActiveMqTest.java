package com.activemq.demo;

import com.activemq.demo.utils.DateUtil;
import com.activemq.demo.message.MessageModel;
import com.activemq.demo.producer.Producer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest(classes = ActivemqApplication.class)
@RunWith(SpringRunner.class)
public class ActiveMqTest {
 
    /**
     * 消息生产者
     */
    @Autowired
    private Producer producer;
 
    /**
     * 及时消息队列测试
     */
    @Test
    public void test(){
        Long adviceRecordId = 8984930030L;
        MessageModel messageModel = new MessageModel();
        messageModel.setTitile("bank_notice");
        messageModel.setMessage("失败后二次通知");
        messageModel.setData(adviceRecordId);//通知ID
        // 发送消息
        producer.send(Producer.DEFAULT_QUEUE, messageModel);
        System.out.println("发送消息成功"+ DateUtil.formatDate(new Date()));
    }
 
    /**
     * 延时消息队列测试
     */
    @Test
    public void test2(){
        for (int i=0;i< 20;i++){
            Long adviceRecordId = 8984930030L;
            MessageModel messageModel = new MessageModel();
            messageModel.setTitile("bank_notice");
            messageModel.setMessage("失败后二次通知");
            messageModel.setData(adviceRecordId);//通知ID

            // 发送延迟消息
            producer.delaySend(Producer.DEFAULT_QUEUE, messageModel, 10000L);
            System.out.println("消息发送成功》》》"+DateUtil.formatDate(new Date()));
        }
        try {
            // 等待100秒，等等消息执行
            Thread.currentThread().sleep(100000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}